package main

import (
	"context"
	_ "embed"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"sync"

	"golang.design/x/clipboard"
)

type ClipboardData struct {
	Format clipboard.Format
	Data   []byte
}

type CurrentFileData struct {
	Data []byte
	Mime string
	Size int64
	Name string
	sync.Mutex
}

type CurrentClipboardData struct {
	ClipboardData
	sync.Mutex
}

var (
	//go:embed fup.html
	FUPHtml []byte
)

func main() {
	port := flag.Int("port", 15234, "Port to host on")
	flag.Parse()

	clipboardDataChan, err := clipboardLoop()

	if err != nil {
		log.Fatal("Clipboard error: ", err)
	}

	currentData := new(CurrentClipboardData)

	currentData.Format = clipboard.FmtText
	currentData.Data = []byte("Copy something to use")

	currentFileData := new(CurrentFileData)

	go (func() {
		for data := range clipboardDataChan {
			currentData.Lock()
			fmt.Printf("new data: %v\n", data.Format)
			currentData.ClipboardData = data
			currentData.Unlock()
		}
	})()

	webSetup(currentData)
	webFileSetup(currentFileData)

	log.Printf("Serving on :%d\n", *port)

	http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)

}

func clipboardLoop() (chan ClipboardData, error) {
	if err := clipboard.Init(); err != nil {
		return nil, err
	}

	resultData := make(chan ClipboardData)

	go (func() {
		for {
			select {
			case imageData := <-clipboard.Watch(context.Background(), clipboard.FmtImage):
				resultData <- ClipboardData{
					clipboard.FmtImage,
					imageData,
				}

			case textData := <-clipboard.Watch(context.Background(), clipboard.FmtText):
				resultData <- ClipboardData{
					clipboard.FmtText,
					textData,
				}
			}
		}
	})()

	return resultData, nil
}

func webFileSetup(currentFileData *CurrentFileData) {

	http.HandleFunc("/fup", func(res http.ResponseWriter, req *http.Request) {

		if req.Method == "POST" {
			log.Println("new upload")
			req.ParseMultipartForm(50 << 20)
			file, header, err := req.FormFile("file")

			if err != nil {
				log.Printf("unable to get file: %v\n", err)
				return
			}
			defer file.Close()

			log.Printf("header %s, %d\n", header.Filename, header.Size)
			currentFileData.Lock()
			defer currentFileData.Unlock()

			currentFileData.Name = header.Filename
			currentFileData.Size = header.Size
			currentFileData.Mime = header.Header.Get("Content-Type")

			currentFileData.Data, err = io.ReadAll(file)
			if err != nil {
				log.Printf("unable to read file: %v\n", err)
			}
		} else if req.Method == "GET" {
			res.Write(FUPHtml)
		}
	})

	http.HandleFunc("/fdn", func(res http.ResponseWriter, req *http.Request) {
		currentFileData.Lock()
		defer currentFileData.Unlock()
		res.Header().Add("Content-Type", currentFileData.Mime)
		res.Header().Add("Content-Disposition", fmt.Sprintf("inline; filename=\"%s\"", currentFileData.Name))
		res.Header().Add("Content-Length", fmt.Sprintf("%d", currentFileData.Size))
		res.Write(currentFileData.Data)
	})
}

func webSetup(currentData *CurrentClipboardData) {

	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		currentData.Lock()
		defer currentData.Unlock()
		if currentData.Format == clipboard.FmtText {
			fmt.Fprintf(res, "%s", currentData.Data)

		} else {
			res.Header().Add("ContentType", "image/png")
			res.Write(currentData.Data)
		}

	})

}
